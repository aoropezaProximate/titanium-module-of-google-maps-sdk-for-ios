var vars = {
	mapView 				: null,
	annotation				: null,
	googlemaps 				: null,
	bearin_angule			: false,
	zoom					: false,
	location				: false,
	maptype					: false,
	changeCameraPosition	: function(e){
		Ti.API.info('changeCameraPosition: '+JSON.stringify(e));
	},
	clickMap				: function(e){
		Ti.API.info('click: '+JSON.stringify(e));
	},
	longpress 				: function(e){
		Ti.API.info('longpress: '+JSON.stringify(e));
	},
	clickMarker				: function(e){
		Ti.API.info('clickMarker: '+JSON.stringify(e));
	},
	infoWindowClickMarker	: function(e){
		Ti.API.info('infoWindowClickMarker: '+JSON.stringify(e));
		alert('Tap on: '+e.source.title);
	}
};

function location(){
	if(vars.location){
		vars.mapView.setLocation({
			latitude : 19.359571,
	  		longitude : -99.185257
		});
	}
	else{
		vars.mapView.setLocation({
			latitude : 19.359571,
	  		longitude : -99.170353
		});
	}
	vars.location = !vars.location;
};
function zoom(){
	if(vars.zoom){
		vars.mapView.setZoom(17);
	}
	else{
		vars.mapView.setZoom(10);
	}
	vars.zoom = !vars.zoom;
};
function bearin_angule(){
	if(vars.bearin_angule){
		var angule = 0;
	}
	else{
		var angule = 60;
	}
	
	setTimeout(function(){
		vars.mapView.setBearing(angule);	
	}, 0);
	setTimeout(function(){
		vars.mapView.setViewingAngle(angule);	
	}, 800);
	
	vars.bearin_angule = !vars.bearin_angule;
};
function maptype(){
	if(vars.maptype){
		vars.mapView.setMapType(vars.googlemaps.NORMAL_TYPE);
	}
	else{
		vars.mapView.setMapType(vars.googlemaps.HYBRID_TYPE);
	}
	vars.maptype = !vars.maptype;
};

$.containerwin.addEventListener('open', function(){
	vars.googlemaps = require('com.proximate.googlemaps.ios');
	vars.googlemaps.setAPIKey("<you Google APIs here>");
	vars.googlemaps.setDebug(true);
	vars.mapView = vars.googlemaps.createMapView({
		top 	 			: 100,
		bottom 	 			: 0,
		zoom 				: 17,
		myLocationEnabled 	: false,
		mapType				: vars.googlemaps.NORMAL_TYPE,
		location 			: {
	  		latitude : 19.359571,
	  		longitude : -99.185257
		}
	});
	
	vars.annotation = vars.googlemaps.createMapAnnotation({
		latitude 		: 19.359571,
	  	longitude 		: -99.185257,
		title 			: 'Tokyo',
		snippet 		: 'hogehoge, hogehoge',
		markerIcon		: 'ico_moto.png',
		cutsomProperty 	: 'isbn-12345'
	});
	vars.annotation.addEventListener('click', vars.clickMarker);
	vars.annotation.addEventListener('infoWindowClick', vars.infoWindowClickMarker);
	vars.mapView.addAnnotation(vars.annotation);
	
	vars.mapView.addEventListener('changeCameraPosition', vars.changeCameraPosition);
	vars.mapView.addEventListener('longpress', vars.longpress);
	vars.mapView.addEventListener('click', vars.clickMap);
	$.containerwin.add(vars.mapView);
});

$.containerwin.open();

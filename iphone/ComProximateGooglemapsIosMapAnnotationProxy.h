/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2014 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */
#import "TiProxy.h"

@interface ComProximateGooglemapsIosMapAnnotationProxy : TiProxy {
    GMSMarker *_markerObj;
}

@property (nonatomic, readwrite, retain) GMSMarker *markerObj;

-(void)addToMap:(GMSMapView *)mapView;
-(void)removeFromMap:(GMSMapView *)mapView;

@end

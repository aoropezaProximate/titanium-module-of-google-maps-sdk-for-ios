//
//  Commons.h
//  proximategooglemapsios
//
//  Created by Alejandro Oropeza on 8/21/14.
//
//

#import <Foundation/Foundation.h>

@interface Commons : NSObject

+(BOOL) getDebugEnable;
+(void)setDebugEnable:(BOOL)_debugEnable;
+(void)printLog:(NSString *)log;


@end

/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2014 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */
#import "TiUIView.h"

@interface ComProximateGooglemapsIosMapView : TiUIView<GMSMapViewDelegate>
{
	GMSMapView * _map;
    
	CLLocationCoordinate2D _location;
	CGFloat _zoom;
	CLLocationDirection _bearing;
	double _angle;
	BOOL _locChanged;
	BOOL _zoomChanged;
	BOOL _bearingChanged;
	BOOL _angleChanged;
    
	BOOL _myLocationEnabled;
    BOOL _myLocationEnabledChange;
    
	BOOL _rendered;
	BOOL _animate;
    
	NSMutableArray* _annotationsAdded; // Annotations to add on initial display
}

-(void)addAnnotation:(id)args;
//-(void)addAnnotations:(id)args;
-(void)removeAnnotation:(id)args;
//-(void)removeAnnotations:(id)args;
-(void)removeAllAnnotations;

@end
/**
 * Appcelerator Titanium Mobile
 * Copyright (c) 2009-2014 by Appcelerator, Inc. All Rights Reserved.
 * Licensed under the terms of the Apache Public License
 * Please see the LICENSE included with this distribution for details.
 */

#import "ComProximateGooglemapsIosMapViewProxy.h"
#import "ComProximateGooglemapsIosMapView.h"
#import "ComProximateGooglemapsIosMapAnnotationProxy.h"

#import "TiUtils.h"

@implementation ComProximateGooglemapsIosMapViewProxy

-(void)addAnnotation:(id)arg
{
	ENSURE_SINGLE_ARG(arg,NSObject);
	ENSURE_UI_THREAD(addAnnotation,arg);
    
    ComProximateGooglemapsIosMapAnnotationProxy* annProxy = arg;
    [self rememberProxy:annProxy];
    
    {
		TiThreadPerformOnMainThread(^{[(ComProximateGooglemapsIosMapView*)[self view] addAnnotation:arg];}, NO);
	}
}

-(void)removeAnnotation:(id)arg
{
	ENSURE_SINGLE_ARG(arg,NSObject);
    ENSURE_UI_THREAD(removeAnnotation,arg);
	
    if ([arg isKindOfClass:[ComProximateGooglemapsIosMapAnnotationProxy class]]) {
		[self forgetProxy:arg];
	}
	{
		TiThreadPerformOnMainThread(^{
            [(ComProximateGooglemapsIosMapView*)[self view] removeAnnotation:arg];
		}, NO);
	}
}

-(void)removeAllAnnotations:(id)arg
{
    arg = @"";//fixed, with this we can use funtions witour args, Is not the best way, bus it's work :)
	ENSURE_SINGLE_ARG(arg,NSObject);
    ENSURE_UI_THREAD(removeAllAnnotations,arg);
	
    if ([arg isKindOfClass:[ComProximateGooglemapsIosMapAnnotationProxy class]]) {
		[self forgetProxy:arg];
	}
    
	{
		TiThreadPerformOnMainThread(^{
            [(ComProximateGooglemapsIosMapView*)[self view] removeAllAnnotations];
		}, NO);
	}
}


@end

/**
 * proximategooglemapsios
 *
 * Created by Your Name
 * Copyright (c) 2014 Your Company. All rights reserved.
 */

#import "ComProximateGooglemapsIosModule.h"
#import "TiBase.h"
#import "TiHost.h"
#import "TiUtils.h"

@implementation ComProximateGooglemapsIosModule

MAKE_SYSTEM_PROP(NORMAL_TYPE, kGMSTypeNormal); // Basic maps.
MAKE_SYSTEM_PROP(SATELLITE_TYPE,kGMSTypeSatellite); // Satellite maps with no labels.
MAKE_SYSTEM_PROP(TERRAIN_TYPE,kGMSTypeTerrain); // Terrain maps.
MAKE_SYSTEM_PROP(HYBRID_TYPE,kGMSTypeHybrid); // Satellite maps with a

#pragma mark Internal

// this is generated for your module, please do not change it
-(id)moduleGUID
{
	return @"ddd8fdbf-ee17-4289-af04-60637162e558";
}

// this is generated for your module, please do not change it
-(NSString*)moduleId
{
	return @"com.proximate.googlemaps.ios";
}

#pragma mark Lifecycle

-(void)startup
{
	// this method is called when the module is first loaded
	// you *must* call the superclass
	[super startup];

	NSLog(@"[INFO] %@ loaded",self);
}

-(void)shutdown:(id)sender
{
	// this method is called when the module is being unloaded
	// typically this is during shutdown. make sure you don't do too
	// much processing here or the app will be quit forceably

	// you *must* call the superclass
	[super shutdown:sender];
}

#pragma mark Cleanup

-(void)dealloc
{
	// release any resources that have been retained by the module
	[super dealloc];
}

#pragma mark Internal Memory Management

-(void)didReceiveMemoryWarning:(NSNotification*)notification
{
	// optionally release any resources that can be dynamically
	// reloaded once memory is available - such as caches
	[super didReceiveMemoryWarning:notification];
}

#pragma mark Listener Notifications

-(void)_listenerAdded:(NSString *)type count:(int)count
{
	if (count == 1 && [type isEqualToString:@"my_event"])
	{
		// the first (of potentially many) listener is being added
		// for event named 'my_event'
	}
}

-(void)_listenerRemoved:(NSString *)type count:(int)count
{
	if (count == 0 && [type isEqualToString:@"my_event"])
	{
		// the last listener called for event named 'my_event' has
		// been removed, we can optionally clean up any resources
		// since no body is listening at this point for that event
	}
}

#pragma Public APIs

-(void)setAPIKey:(id)value
{
	[GMSServices provideAPIKey:[TiUtils stringValue:value]];
}

-(void)setDebug:(id)value
{
	[Commons setDebugEnable:[TiUtils boolValue:value]];
}

@end

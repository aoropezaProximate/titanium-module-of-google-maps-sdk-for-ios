//
//  Commons.m
//  proximategooglemapsios
//
//  Created by Alejandro Oropeza on 8/21/14.
//
//

#import "Commons.h"

@implementation Commons

static BOOL *debugEnable = FALSE;

+(BOOL) getDebugEnable{
    return debugEnable;
}
+(void)setDebugEnable:(BOOL)_debugEnable{
    debugEnable = _debugEnable;
}

+(void)printLog:(NSString *)log{
    if (debugEnable) {
        NSLog(@"[LOG(proximate.google.maps.ios)] %@",log);
    }
}

@end
